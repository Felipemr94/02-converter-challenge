//
//  ContentView.swift
//  UnitConverter
//
//  Created by Felipe Marques Ramos on 2/19/22.
//

import SwiftUI

struct ContentView: View {

    @State private var input = ""
    @State private var selectedInputUnit = "Meters"
    @State private var selectedOutputUnit = "Meters"

    let units = [
        "Megameters",
        "Kilometers",
        "Hectometers",
        "Decameters",
        "Meters",
        "Decimeters",
        "Milimeters",
        "Micrometers",
        "Nanometers",
        "Picometers",
        "Inches",
        "Feet",
        "Yards",
        "Miles",
        "Natical Miles",
        "Fathoms",
        "Furlongs",
        "Astronomical Units",
        "Parsecs"
    ]

    private var output:Double {
        let valueInMeters = convertToMeters(from: selectedInputUnit, value: (Double(input) ?? 0.0))
        return convertFromMeters(to: selectedOutputUnit, value: valueInMeters)
    }

    var body: some View {
        Form {
            Section {
                TextField("Enter length", text: $input)
                    .keyboardType(.decimalPad)
                Picker("Select Unit", selection: $selectedInputUnit) {
                    ForEach(units, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(.menu)
            } header: {
                Text("Input")
            }
            Section {
                Text(output.formatted())
                Picker("Select Unit", selection: $selectedOutputUnit) {
                    ForEach(units, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(.menu)
            } header: {
                Text("Output")
            }
        }
    }

    func convertToMeters(from unit: String, value: Double) -> Double{

        var measurement = Measurement(value: value, unit: UnitLength.meters)

        switch unit {
        case "Megameters":
            measurement = Measurement(value: value, unit: UnitLength.megameters)
        case "Kilometers":
            measurement = Measurement(value: value, unit: UnitLength.kilometers)
        case "Hectometers":
            measurement = Measurement(value: value, unit: UnitLength.hectometers)
        case "Decameters":
            measurement = Measurement(value: value, unit: UnitLength.decameters)
        case "Meters":
            measurement = Measurement(value: value, unit: UnitLength.meters)
        case "Decimeters":
            measurement = Measurement(value: value, unit: UnitLength.decimeters)
        case "Milimeters":
            measurement = Measurement(value: value, unit: UnitLength.millimeters)
        case "Micrometers":
            measurement = Measurement(value: value, unit: UnitLength.micrometers)
        case "Nanometers":
            measurement = Measurement(value: value, unit: UnitLength.nanometers)
        case "Picometers":
            measurement = Measurement(value: value, unit: UnitLength.picometers)
        case "Inches":
            measurement = Measurement(value: value, unit: UnitLength.inches)
        case "Feet":
            measurement = Measurement(value: value, unit: UnitLength.feet)
        case "Yards":
            measurement = Measurement(value: value, unit: UnitLength.yards)
        case "Miles":
            measurement = Measurement(value: value, unit: UnitLength.miles)
        case "Natical Miles":
            measurement = Measurement(value: value, unit: UnitLength.nauticalMiles)
        case "Fathoms":
            measurement = Measurement(value: value, unit: UnitLength.fathoms)
        case "Furlongs":
            measurement = Measurement(value: value, unit: UnitLength.furlongs)
        case "Astronomical Units":
            measurement = Measurement(value: value, unit: UnitLength.astronomicalUnits)
        case "Parsecs":
            measurement = Measurement(value: value, unit: UnitLength.parsecs)
        default :
            measurement = Measurement(value: value, unit: UnitLength.meters)
        }

        return measurement.converted(to: UnitLength.meters).value
    }
    func convertFromMeters(to unit: String, value: Double) -> Double{

        let measurement = Measurement(value: value, unit: UnitLength.meters)

        switch unit {
        case "Megameters":
            return measurement.converted(to: UnitLength.megameters).value
        case "Kilometers":
            return measurement.converted(to: UnitLength.kilometers).value
        case "Hectometers":
            return measurement.converted(to: UnitLength.hectometers).value
        case "Decameters":
            return measurement.converted(to: UnitLength.decameters).value
        case "Meters":
            return measurement.converted(to: UnitLength.meters).value
        case "Decimeters":
            return measurement.converted(to: UnitLength.decimeters).value
        case "Milimeters":
            return measurement.converted(to: UnitLength.millimeters).value
        case "Micrometers":
            return measurement.converted(to: UnitLength.micrometers).value
        case "Nanometers":
            return measurement.converted(to: UnitLength.nanometers).value
        case "Picometers":
            return measurement.converted(to: UnitLength.picometers).value
        case "Inches":
            return measurement.converted(to: UnitLength.inches).value
        case "Feet":
            return measurement.converted(to: UnitLength.feet).value
        case "Yards":
            return measurement.converted(to: UnitLength.yards).value
        case "Miles":
            return measurement.converted(to: UnitLength.miles).value
        case "Natical Miles":
            return measurement.converted(to: UnitLength.nauticalMiles).value
        case "Fathoms":
            return measurement.converted(to: UnitLength.fathoms).value
        case "Furlongs":
            return measurement.converted(to: UnitLength.furlongs).value
        case "Astronomical Units":
            return measurement.converted(to: UnitLength.astronomicalUnits).value
        case "Parsecs":
            return measurement.converted(to: UnitLength.parsecs).value
        default :
            return measurement.converted(to: UnitLength.meters).value
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
