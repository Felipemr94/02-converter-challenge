//
//  UnitConverterApp.swift
//  UnitConverter
//
//  Created by Felipe Marques Ramos on 2/19/22.
//

import SwiftUI

@main
struct UnitConverterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
